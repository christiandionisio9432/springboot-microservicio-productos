package com.formacionbdi.springbootservicioproductos.models.dao;

import com.formacionbdi.springbootservicioproductos.models.entity.Producto;
import org.springframework.data.repository.CrudRepository;

public interface ProductoDao extends CrudRepository<Producto, Long> {
}
