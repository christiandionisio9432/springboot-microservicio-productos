package com.formacionbdi.springbootservicioproductos.controllers;

import com.formacionbdi.springbootservicioproductos.models.entity.Producto;
import com.formacionbdi.springbootservicioproductos.models.service.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProductoController {
	
	@Autowired
	private Environment env;
	
//	@Value("${server.port}")
//	private Integer port;

    @Autowired
    private ServletWebServerApplicationContext webServerAppCtxt;

    @Autowired
    private IProductoService service;

    @GetMapping("/listar")
    public List<Producto> listar() {
        return service.findAll().stream()
        		.map(producto -> {
        			// producto.setPort(Integer.parseInt(env.getProperty("local.server.port")));
        			producto.setPort(webServerAppCtxt.getWebServer().getPort());
        			return producto;
        		}).collect(Collectors.toList());
    }

    @GetMapping("/ver/{id}")
    public Producto detalle(@PathVariable Long id) {
        Producto producto =  service.findById(id);
        // producto.setPort(Integer.parseInt(env.getProperty("local.server.port")));
        producto.setPort(webServerAppCtxt.getWebServer().getPort());
        return producto;
    }

}
